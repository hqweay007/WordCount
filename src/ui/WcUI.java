package ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * 启动类
 */
public class WcUI extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    Parent root = null;
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Ui.fxml"));
    try {
      root = fxmlLoader.load();
    } catch (IOException e) {
      e.printStackTrace();
    }
    FileChooser fileChooser = new FileChooser();

    WcController controller = fxmlLoader.getController();

    controller.setPrimaryStage(primaryStage);
    primaryStage.setTitle("WordCount");
    primaryStage.setScene(new Scene(root));

    primaryStage.show();
  }
}
