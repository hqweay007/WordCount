package ui;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import program.Program;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 界面请求控制器
 *
 * @author zhangbo
 */
public class WcController {
  final Tooltip tooltip = new Tooltip();

  @FXML
  private TextField regexField;

  @FXML
  private CheckBox isRecursion;

  @FXML
  private TextField stopFile;

  @FXML
  private TextField inputFile;

  @FXML
  private CheckBox isCountLine;

  @FXML
  private CheckBox isCountChar;

  @FXML
  private CheckBox isCountWord;

  @FXML
  private CheckBox countCodeInfo;

  @FXML
  private TextField resultFile;


  private Stage primaryStage;

  private static HashMap<String, String> args;

  static {
    args = new HashMap();
    args.put("isCountLine", "-l");
    args.put("isCountWord", "-w");
    args.put("isCountChar", "-c");
    args.put("countCodeInfo", "-a");
    args.put("isRecursion", "-s");
//        args.put("-e");
  }

  /**
   * 响应 button的点击事件
   */
  public void textProcess() {
    ArrayList<String> params = new ArrayList<>();
    if (isCountLine.isSelected()) {
      // sb.append(args.get(isCountLine.getId())).append(" ");
      params.add(args.get(isCountLine.getId()));
    }
    if (isCountChar.isSelected()) {
      //sb.append(args.get(isCountChar.getId())).append(" ");
      params.add(args.get(isCountChar.getId()));
    }
    if (isCountWord.isSelected()) {
      //  sb.append(args.get(isCountWord.getId())).append(" ");
      params.add(args.get(isCountWord.getId()));
    }
    if (countCodeInfo.isSelected()) {
      //  sb.append(args.get(countCodeInfo.getId())).append(" ");
      params.add(args.get(countCodeInfo.getId()));
    }
    if (isRecursion.isSelected()) {
      //   sb.append(args.get(isRecursion.getId())).append(" ");
      //追加 过滤条件
      params.add(args.get(isRecursion.getId()));
    }
    if (!"".equals(stopFile.getText())) {
      //   sb.append("-e").append(" ").append(stopFile.getText()).append(" ");
      params.add("-e");
      params.add(stopFile.getText());
    }
    if (!"".equals(resultFile.getText())) {
      // sb.append("-o").append(" ").append(resultFile.getText()).append(" ");
      params.add("-o");
      params.add(resultFile.getText());
    }
    if (!"".equals(inputFile.getText())) {
      //   sb.append(inputFile.getText()).append(" ");
      params.add(inputFile.getText());
    }

    int size = params.size();
    String[] array = params.toArray(new String[size]);

    Program.main(array);
    System.out.print(params);
  }

  /**
   * 递归选项勾选   后面的文本框编程可编辑
   */
  public void changeTextState() {
    regexField.setEditable(true);
  }

  public void isCheckRecursion() {
    if (!isRecursion.isSelected()) {
//            tooltip.setText(
//                    "\n您必须要勾选递归处理\n" + "才能设置过滤条件\n");
//          //  System.out.print("nonoonono");
//            regexField.setTooltip(tooltip);
      Alert alert = new Alert(Alert.AlertType.INFORMATION);
      alert.titleProperty().set("信息");
      alert.headerTextProperty().set("您必须要勾选递归处理才能设置过滤条件");
      alert.showAndWait();
    }
  }

  /**
   * @param desc      描述
   * @param extension 规则
   * @return
   */
  public File chooseFile(String desc, String... extension) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("选择");
    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    // if (extension != null) {
    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(desc, extension);
    //  }
    fileChooser.getExtensionFilters().add(extFilter);
    File file = fileChooser.showOpenDialog(primaryStage);
    return file;
  }

  /**
   * 选择停止词文件
   *
   * @return
   */
  public void chooseStopFile() {
    if (!"".equals(stopFile.getText())) {
      return;
    }
    File file = chooseFile("null", "*.*");

    if (file != null) {
      stopFile.setText(file.getPath());
    }

  }


  /**
   * 选择文件或者目录
   *
   * @return
   */
  public void chooseInput() {
    File file = null;
    if (isRecursion.isSelected()) {
      DirectoryChooser directoryChooser = new DirectoryChooser();
      file = directoryChooser.showDialog(primaryStage);
    } else {
      file = chooseFile("null", "*.*");
    }

    if (file != null) {
      inputFile.setText(file.getPath());
    }

  }


  public Stage getPrimaryStage() {
    return primaryStage;
  }

  public void setPrimaryStage(Stage primaryStage) {
    this.primaryStage = primaryStage;
  }
}
